//JScript Class 1 - Variables and Data Types
// Numbers 
// You Do These 
// Convert Binary / Decimal 
//Convert Binary / Decimal: 
/*
1001101 = 77 
1.011 = 1.375
1100011 = 99 
0.0001100110011001101 = 0.1
*/

// Math Practice 

/*1.Using the Math object, put together a code snippet that allows you to draw a random card with a value between 1 and 13 (assume ace is 1, jack is 11...)*/

Math.floor(Math.random()*13) + 1 ; 

/*2. Draw 3 cards and use Math to determine the highest card.*/
let cardOne = Math.floor(Math.random()*13) + 1 ; 
let cardTwo = Math.floor(Math.random()*13) + 1 ; 
let cardThree = Math.floor(Math.random()*13) + 1 ; 
Math.max(cardOne, cardTwo, cardThree); 

/*3. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.  What is the surface area for each of these pizzas?*/
let areaSmallPizza = Math.PI * Math.pow((13/2), 2); 
let areaLargePizza = Math.PI * Math.pow((17/2), 2); 

//4.What is the cost per square inch of each pizza?
let smallizzaPrice = 16.99/areaSmallPizza
smallPizzaPrice.toFixed(2); 
let largePizzaPrice = 19.99/areaLargePizza
largePizzaPrice.toFixed(2); 

//Address Line 

//1.Create variables for firstName, lastName, streetAddress, city, state, and zipCode.  Use this information to create a formatted address block that could be printed onto an envelope.
let firstName = "Alycia"; 
let lastName = "Cahill"; 
let street = 123 + " Mulberry Street";
let city= "Seattle"; 
let state = "Wa"; 
let zipCode = 98117; 

let envalope = firstName + ' ' + lastName + '\n' + street + '\n' + city + ', ' + state + ' ' + zipCode; 

//2. You are given a string in this format:firstNamelastName(assume no spaces in either)streetAddresscity, state zip (could be spaces in city and state)–Write code that is able to extract this full string into each variable.  Hint: use indexOf, slice, and/or substring
let str="Micheal Scott/n1725 Slough Avenue/nScranton, PA 18540"; 
let space = ' ';
let line = '/n'; 
let comma =','; 
let cityStateZip = str.slice(str.lastIndexOf(line)+2, str.length); 
let firstName = str.slice(0, str.indexOf(space)); 
let lastName = str.slice(str.indexOf(space), str.indexOf(line)); 
let street = str.substring(str.indexOf(line)+2, str.lastIndexOf(line)); 
let city = cityStateZip.slice(0, cityStateZip.indexOf(comma)); 
let state = cityStateZip.slice(cityStateZip.indexOf(space)+1, cityStateZip.lastIndexOf(space)); 
let zip = cityStateZip.slice(cityStateZip.lastIndexOf(space)+1, cityStateZip.length); 

//Find the Middle Date 

let endDate = new Date(2019,3,1); 
let startDate = new Date (2019, 0, 1); 
let middleDate = new Date((startDate.getTime() + endDate.getTime()) / 2); 
